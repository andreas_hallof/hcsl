# Hashbased Certificate Status List (HCSL)

Eine CA veröffentlicht zwei signierte Status-Liste über seine EE-Zertifikate:
einmal eine Negativliste und einmal eine Positiv-Liste.

Alle 10 Minuten werden die Listen akualisiert (neu erzeugt) und zusätzlich 
werden Diff-Dateien (vor-10-Minuten-Datei vs. jetzt-Datei) erzeugt.

Die Diff-Dateien sind dann nur bspw. 

    6 Byte * Anzahl-neu-gesperrte-Zertifikate + Signatur-Wert

groß.

## Beispiel

Ich erzeuge zufällig Testdaten (1 Millionen EE-Zertifikate, 20% davon sind gesperrt).

    $ ./gen_certdb.py 1_000_000 0.2
    using parameters: nr#cert=1000000 revoked_ratio=0.2
    100% (800000 of 800000) |##################################| Elapsed Time: 0:00:03 Time:  0:00:03
    100% (200000 of 200000) |##################################| Elapsed Time: 0:00:00 Time:  0:00:00
    #good    = 800000
    #revoked = 200000
    ratio    = 0.2

Ich erzeuge die Basis-Negativ und -Positiv-Listen 

    $ time ./create-hnl.py
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    100% (200000 of 200000) |#######################| Elapsed Time: 0:00:00 Time:  0:00:00
    Sanity Check: sind die good-Menge und die revoked-Menge wirklich disjunkt
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    Prefixlen 1
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    positiv prefix set size: 256
      0% (0 of 200002) |                            | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 2
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    positiv prefix set size: 65536
      0% (0 of 200002) |                            | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 3
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    positiv prefix set size: 781314
      0% (0 of 200002) |                            | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 4
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    positiv prefix set size: 799934
      0% (0 of 200002) |                            | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 5
    100% (800000 of 800000) |#######################| Elapsed Time: 0:00:01 Time:  0:00:01
    positiv prefix set size: 799999
    100% (200002 of 200002) |#######################| Elapsed Time: 0:00:00 Time:  0:00:00
    Final: 5
    100% (200002 of 200002) |#######################| Elapsed Time: 0:00:00 Time:  0:00:00
    Hauptteil hat 200002 Elemente
    So die Daten sind berechnet ... 
    Ich erzeuge jetzt die Datenstukturen und die zwei Signaturen ...
    100% (200002 of 200002) |#######################| Elapsed Time: 0:00:00 Time:  0:00:00
    Signatur 1 erstellen
    Signatur 2 erstellen
    L_0 erstellen
    100% (200001 of 200001) |#######################| Elapsed Time: 0:00:00 Time:  0:00:00

    real    0m14.903s
    user    0m12.218s
    sys     0m1.202s

    $ ll tihnl1-*
    -rw-r--r-- 1 Andreas.Hallof Domänen-Benutzer 1000215 Jun 19 11:36 tihnl1-1718789802-54bc93f447a363a6289e11df150ca93f9155071d

    $ time ./create-hpl.py
    100% (800000 of 800000) |#################################| Elapsed Time: 0:00:01 Time:  0:00:01
    100% (200000 of 200000) |#################################| Elapsed Time: 0:00:00 Time:  0:00:00
    Sanity Check: sind die good-Menge und die revoked-Menge wirklich disjunkt
    100% (800000 of 800000) |#################################| Elapsed Time: 0:00:01 Time:  0:00:01
    Prefixlen 1
    100% (200000 of 200000) |#################################| Elapsed Time: 0:00:00 Time:  0:00:00
    rpl: 256
      0% (0 of 800000) |                                      | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 2
    100% (200000 of 200000) |####################| Elapsed Time: 0:00:00 Time:  0:00:00
    rpl: 62464
      0% (0 of 800000) |                                      | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 3
    100% (200000 of 200000) |#################################| Elapsed Time: 0:00:00 Time:  0:00:00
    rpl: 198823
      0% (0 of 800000) |                                      | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 4
    100% (200000 of 200000) |#################################| Elapsed Time: 0:00:00 Time:  0:00:00
    rpl: 199995
      0% (0 of 800000) |                                      | Elapsed Time: 0:00:00 ETA:  --:--:--
    Prefixlen 5
    100% (200000 of 200000) |#################################| Elapsed Time: 0:00:00 Time:  0:00:00
    rpl: 200000
    100% (800000 of 800000) |#################################| Elapsed Time: 0:00:01 Time:  0:00:01
    Final: 5
    100% (800000 of 800000) |#################################| Elapsed Time: 0:00:01 Time:  0:00:01
    Hauptteil hat 799999 Elemente
    So die Daten sind berechnet ...
    Ich erzeugte jetzt die Datenstukturen und die zwei Signaturen ...
    100% (799999 of 799999) |#################################| Elapsed Time: 0:00:01 Time:  0:00:01
    Signatur 1 erstellen
    Signatur 2 erstellen

    real    0m13.544s
    user    0m11.062s
    sys     0m0.998s

    $ ll tihpl1-*
    -rw-r--r-- 1 Andreas.Hallof Domänen-Benutzer 4000201 Jun 19 11:38 tihpl1-1718789902-54bc93f447a363a6289e11df150ca93f9155071d

Jetzt sperre ich als CA mal 10^3 Zertifikate und füge 10^4 Zertifikate hinzu:

    $ ./remove_cert.py revoked.db 1000
    removing 1000 element(s) from good.db
    filesize now 25568000

    $ ./add_cert.py good.db 10_000
    adding 10000 element(s) to good.db
    Filesize now 25888000

    $ ./info_certdb.py
    #good    = 809000
    #revoked = 200000
    ratio=0.19821605550049554


Ich erzeuge neue Positiv- und Negativlisten (HCSL). Und erzeuge jeweils die Diff-Dateien, die ich mir
hier mal ansehe:


    $ ./create-diff.py tihnl1-1718790*
    starttime = 1718790422
    dst-time  = 1718790637
    type      = tihnl1
    hlen      = 5
    aki       = 54bc93f447a363a6289e11df150ca93f9155071d
    100% (200002 of 200002) |############################| Elapsed Time: 0:00:00 Time:  0:00:00
    100% (200002 of 200002) |############################| Elapsed Time: 0:00:00 Time:  0:00:00
    0 elements must be removed
    100% (200002 of 200002) |############################| Elapsed Time: 0:00:00 Time:  0:00:00
    0 elements must be added
    - |#                                                                                                                                              | 0 Elapsed Time: 0:00:00
    total size of diff-file: 235
    
    $ ll tidiff1-tihnl1-1718790422-1718790637-54bc93f447a363a6289e11df150ca93f9155071d
    -rw-r--r-- 1 Andreas.Hallof Domänen-Benutzer 235 Jun 19 11:54 tidiff1-tihnl1-1718790422-1718790637-54bc93f447a363a6289e11df150ca93f9155071d

Die Diff-Datei ist also nur 235 Byte groß.

