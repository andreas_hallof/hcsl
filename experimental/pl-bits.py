#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Ich geht hier mal auf Bit-Ebene, was wir in Normafall micht tun.
Abwägung Kosten-Nutzen

"""

import os, hashlib, sys, time

from binascii import hexlify, unhexlify
from progressbar import progressbar

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import utils


if __name__ == '__main__':

    with open("../certdb/good.db", "rb") as good_file:
        data = good_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        good = []
        for i in progressbar(range(0, nr)):
            s = bin(int.from_bytes(data[i*32:(i+1)*32], byteorder="big", signed=False))
            s = s[2:]
            s = s.zfill(256)
            assert len(s) == 256
            good.append(s)

    with open("../certdb/revoked.db", "rb") as revoked_file:
        data = revoked_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        revoked = []
        for i in progressbar(range(0, nr)):
            s = bin(int.from_bytes(data[i*32:(i+1)*32], byteorder="big", signed=False))
            s = s[2:]
            s = s.zfill(256)
            assert len(s) == 256
            revoked.append(s)

    hlen = 1
    conflict = True
    while conflict:
        print("Prefixlen", hlen)
        revoked_prefix_set = set()
        for element in progressbar(revoked):
            revoked_prefix_set.add(element[0:hlen])
        print("negative prefix set size:", len(revoked_prefix_set), 2**hlen)
        test_conflict = False
        for element in progressbar(good):
            if element[0:hlen] in revoked_prefix_set:
                test_conflict = True
                break
        if test_conflict:
            hlen += 1
        else:
            conflict = False

    print("Final:", hlen)

    my_set = set()
    for element in progressbar(good):
        my_set.add(element[0:hlen])

    my_list = sorted(my_set)

    print("Hauptteil hat", len(my_list), "Elemente")

    with open("res-pl-bits.txt", "wt") as f:
        f.write(f"{hlen} {len(my_set)}\n")

