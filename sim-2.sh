#! /bin/bash

[ X$HOME != X ] || exit 1

LOG=$HOME/git/hcsl/log.sim-2

for i in $(seq 0 100); do
    ratio=$(perl -E "print($i/100)")
    echo $i $ratio
    cd certdb || exit 1
    ./gen_certdb.py 500000 $ratio
    cd ..
    rm tihnl1-* tihpl1-*
    ./create-hpl.py
    ./create-hnl.py
    s1=$(stat -c %s tihpl1-*)
    hlen1=$(./list_info.py tihpl1-* |grep hlen | cut -c8-)
    s2=$(stat -c %s tihnl1-*)
    hlen2=$(./list_info.py tihnl1-* |grep hlen | cut -c8-)
    echo $i $ratio $s1 $hlen1 $s2 $hlen2 >>$LOG
done

