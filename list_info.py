#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Create HPL using the data from certdb/{good.db,revoked.db}

"""

import os, hashlib, sys, time, tomli

from binascii import hexlify, unhexlify

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import utils


if __name__ == '__main__':

    if len(sys.argv) == 1:
        sys.exit("Ich erwarte als erste Argument den Dateinamen einer Liste.")

    filename = sys.argv[1]
    if not (os.path.exists(filename) and os.path.isfile(filename)):
        sys.exit(f"Datei {filename} nicht gefunden")

    # xxx zahl stimmt noch nicht
    if os.stat(filename).st_size<(60+8+2*20+1+8+32*2):
        sys.exit(f"Datei zu klein")

    filename_ar = filename.split("-")
    if len(filename)!= 58 or len(filename_ar)!=3:
        print("WARNING Namenskonvention Dateiname nicht eingehalten")
        filename_test = False
    else:
        filename_test = True

    with open(filename, "rb") as f:
        my_type = f.read(6).decode()
        if not(my_type == "TIHPL1" or my_type == "TIHNL1"):
            sys.exit("Prefix im Header falsch")
        if filename_test and my_type.lower() != filename_ar[0]:
            print("WARNING Namenskonvention: Typ passt nicht")

        print(my_type.lower())
        my_time = int.from_bytes(f.read(8), byteorder='big', signed=False)
        print("Erstellungszeit =", my_time, 
              "({})".format(time.strftime("%Y-%m-%d-%H:%M:%S",
                time.localtime(my_time))))
        if filename_test and str(my_time) != filename_ar[1]:
            print("WARNING Namenskonvention: Zeit passt nicht")
        aki = f.read(20); print("AKI =", hexlify(aki).decode())
        ski = f.read(20); print("SKI =", hexlify(ski).decode())
        hlen = int.from_bytes(f.read(1), byteorder='big', signed=False)
        print("hlen =", hlen)
        ctr = int.from_bytes(f.read(8), byteorder='big', signed=False)
        list_data = f.read(ctr*hlen)
        assert len(list_data) == ctr*hlen
        print("Anzahl Präfixelemente =", ctr)
        trailer_size = os.stat(filename).st_size - f.tell()
        print("Tailersize =", trailer_size)
        trailer = f.read()
        print(hexlify(trailer))

        signature_validation = False
        configfilename = os.environ.get("HOME")
        if not configfilename:
            print("no signatur-validation 1")
        else:
            configfilename = os.path.join(configfilename,".certinfo.toml")
            if not (os.path.exists(configfilename) and os.path.isfile(configfilename)):
                print("no signatur-validation 2")
            else:
                with open(configfilename, "rb") as configfile:
                    certdb = tomli.load(configfile)

    sys.exit(0)

    AKI = "54:BC:93:F4:47:A3:63:A6:28:9E:11:DF:15:0C:A9:3F:91:55:07:1D".replace(":","")
    SKI = "3A:F9:F5:C5:8C:2C:0E:E8:6A:85:ED:7C:44:19:07:FF:72:BB:EC:B8".replace(":","")
    assert len(AKI) == 20*2; assert len(SKI) == 20*2
    TIME = int(time.time())

    with open("certdb/good.db", "rb") as good_file:
        data = good_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        good = []
        for i in progressbar(range(0, nr)):
            good.append(data[i:i+32])

    with open("certdb/revoked.db", "rb") as revoked_file:
        data = revoked_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        revoked = []
        for i in progressbar(range(0, nr)):
            revoked.append(data[i:i+32])

    hlen = 1
    conflict = True
    while conflict:
        print("Prefixlen", hlen)
        revoked_prefix_set = set()
        for element in progressbar(revoked):
            revoked_prefix_set.add(element[0:hlen])
        print("rpl:", len(revoked_prefix_set))
        test_conflict = False
        for element in progressbar(good):
            if element[0:hlen] in revoked_prefix_set:
                test_conflict = True
                break
        if test_conflict:
            hlen += 1
        else:
            conflict = False

    print("Final:", hlen)

    my_set = set()
    for element in progressbar(good):
        my_set.add(element[0:hlen])

    my_list = sorted(my_set)

    print("Hauptteil hat", len(my_list), "Elemente")

    print("So die Daten sind berechnet ... Ich erzeugte jetzt die Datenstukturen und die zwei Signaturen")

    header = b"TIHPL1" + \
             TIME.to_bytes(8, byteorder='big', signed=False) + \
             unhexlify(AKI) + \
             unhexlify(SKI) + \
             hlen.to_bytes(1, byteorder="big", signed=False) 
    nr = len(my_list)
    body_0 = nr.to_bytes(8, byteorder="big", signed=False)
    body_1 = bytearray(nr*hlen)
    counter = 0
    for element in progressbar(my_list):
        body_1[counter:counter+hlen] = element
        counter += 1
    body = body_0 + body_1

    print("Signatur 1 erstellen")
    with open("pki/ocsp_key.pem", "rb") as key_file:
        serialized_private_key=key_file.read()
    private_key = serialization.load_pem_private_key(serialized_private_key,
                              password=None, backend=default_backend() )
    signature_1 = private_key.sign(hashlib.sha256(header+body).digest(),
                                ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    print("Signatur 2 erstellen")
    ende_liste = my_list
    while len(ende_liste)>1:
        start_liste = ende_liste
        l_s = len(start_liste)

        ende_liste = []
        for i in range(0, l_s if l_s & 1 == 0 else l_s-1, 2):
            ende_liste.append(hashlib.sha256(start_liste[i] + start_liste[i+1]).digest())
        if l_s & 1 == 1:
            ende_liste.append(start_liste[-1])

    signature_2 = private_key.sign(hashlib.sha256(header+ende_liste[0]).digest(),
                                ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    signature = signature_1 + signature_2

    with open(f"tihpl1-{TIME}-{AKI}", "wb") as hpl_file:
        hpl_file.write(header + body + signature)

