#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import datetime, hashlib, json, math, sys, time

from binascii import hexlify
from collections import defaultdict
from progressbar import progressbar 


def durchlauf(prozent: float)-> bool:

    assert (prozent >= 0) and (prozent <= 1)

    print(f"Durchlauf mit {prozent}*{MAX} gesperrten Zertifikaten")

    print("Generating Cert-DB Part 2")
    grenze = math.ceil(len(hashlist)*prozent)
    revoked = hashlist[:grenze]
    good = hashlist[grenze:]

    max_cache_letters = 14
    prefix_good_cache = [set() for i in range(0,max_cache_letters)]
    print("building prefix-cache")
    for current_cert_hash in progressbar(good):
        for i in range(0,max_cache_letters):
            prefix_good_cache[i].add(current_cert_hash[:i+1])

    filter_ar = []
    for current_cert_hash in progressbar(revoked):
        my_filter = ""
        found_prefix_is_ok = False
        for letter in current_cert_hash[:max_cache_letters]:
            my_filter += letter
            if not (my_filter in prefix_good_cache[len(my_filter)-1]):
                found_prefix_is_ok = True
                break
        assert found_prefix_is_ok
        filter_ar.append(my_filter)

    print("Länge Filter-Array", len(filter_ar))

    histogramm = defaultdict(lambda: 0)
    for my_item in filter_ar:
        histogramm[len(my_item)]+=1
    print()
    for key in sorted(histogramm):
        print(key,":", histogramm[key])

    max_prefix_len = sorted(histogramm)[-1]
    print("=> Bytelänge max. prefixlänge", "{:.2f}".format(max_prefix_len/2.0))

    print("final check")
    test_uniq = set(filter_ar)

    l_set = len(test_uniq)
    l_ar = len(filter_ar)
    print("assertion")
    print("len set =", l_set, "len filter array=", l_ar, "Ratio=", l_set/float(l_ar))

    byte_per_element = int(math.ceil(max_prefix_len/2.0))
    print(f"{byte_per_element=}")

    mal_zwei = 2*byte_per_element

    final_set = set()
    for revoked_cert in revoked:
        final_set.add(revoked_cert[:mal_zwei])

    l_final_set = len(final_set)
    print("Länge", l_final_set)

    with open("report-" + start_date, "at+") as report_file:
        report_file.write(json.dumps({
            "anzahl" : len(hashlist),
            "prozent" : prozent, 
            "max_prefix_len" : max_prefix_len,
            "histogramm" : histogramm,
            "l_final_set": l_final_set,
            "byte_len_filter" : l_final_set*byte_per_element}) + "\n")

    return True


if __name__ == '__main__':

    MAX = 1_000_000

    start_date = datetime.datetime.now().isoformat()
    print("Generating Cert-DB Part 1")
    hashlist = [hashlib.sha256(f"{i}".encode()).hexdigest() \
                 for i in progressbar(range(0,MAX))]

    for i in range(1,100):
       durchlauf(i/100.0)

    #durchlauf(0.1)

    print("ende")

