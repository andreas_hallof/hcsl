#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import hashlib, math, sys, time

from binascii import hexlify
from collections import defaultdict
from progressbar import progressbar 

if __name__ == '__main__':

    MAX = 100_000

    hashlist = [hashlib.sha256(f"{i}".encode()).hexdigest() for i in range(0,MAX)]

    good = []; revoked = []; i = 1;
    for certhash in sorted(hashlist):
        if i % 10 == 0:
            revoked.append(certhash)
        else:
            good.append(certhash)
        i += 1

    filter_ar = []
    for current_cert_hash in progressbar(revoked):
        my_filter = ""
        for letter in current_cert_hash:
            my_filter += letter
            filter_is_good = True
            for i in good:
                if i.startswith(my_filter):
                    filter_is_good = False
                    break
            if filter_is_good:
                break
        #print(my_filter, filter_is_good)
        filter_ar.append(my_filter)

    print(len(filter_ar))
    print(filter_ar)

    histogramm = defaultdict(lambda: 0)
    for my_item in filter_ar:
        histogramm[len(my_item)]+=1
    print(histogramm)


    test_uniq = set(filter_ar)
    assert len(filter_ar) == len(test_uniq)

