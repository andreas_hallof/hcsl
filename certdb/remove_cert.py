#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Nimmt einen Hashwert (also ein Zertifikat) aus der 
angegebenen db-Datei.

"""

import os, sys

if __name__ == '__main__':

    if len(sys.argv) == 3:
        (filename, nr_certs) = (sys.argv[1], int(sys.argv[2]))
        assert nr_certs > 0
    else:
        filename = "revoked.db"
        nr_certs = 1

    print(f"removing {nr_certs} element(s) from {filename}")

    with open(filename, "ab") as my_file:
        my_file.seek(0, os.SEEK_END)
        my_size = my_file.tell()
        if my_size-32*nr_certs<0:
            print("WARNING: file to small to truncate")
        else:
            my_file.seek(my_size-32*nr_certs)
            my_file.truncate()
            curr_size = my_file.tell()
            print(f"filesize now {curr_size} ({curr_size >> 5} Elements)")

