#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import datetime, hashlib, json, math, sys, time
import matplotlib.pyplot as plt

from collections import defaultdict

if __name__ == '__main__':

    for filename in ["1M-log.sim-1", "2M-log.sim1", "450K-log.sim-1", "500K-log.sim-1", "5M-log.sim1", "10M-log.sim1"]:

        print(filename)

        ratio = []; p_size = []; n_size = []

        with open(filename, "rt") as data_file:
            for line in data_file:
                (a,b,c) = line.split(" ")
                ratio.append(int(a))
                p_size.append(int(b))
                n_size.append(int(c))

        #fig = plt.figure()
        #mytext = fig.add_subplot()
        #mytext.text(0.8, 600, f"TSP mit {job_number} Zertifikaten")

        plt.plot(ratio, p_size, n_size)
        plt.xlabel('Anteil gesperrter Zertifikate')
        plt.ylabel('Größe Filter in Bytes')
        anzahl = filename[0:filename.find("-")]
        plt.text(20, 3000, f"TSP mit {anzahl} Zertifikaten")
        plt.savefig(filename+".png", dpi=300)
        plt.clf()

