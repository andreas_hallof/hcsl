#! /bin/bash

[ X$HOME != X ] || exit 1

LOG=$HOME/git/hcsl/log.sim-1

for i in $(seq 0 100); do
    ratio=$(perl -E "print($i/100)")
    echo $i $ratio
    cd certdb || exit 1
    ./gen_certdb.py 10000000 $ratio
    cd ..
    rm tihnl1-* tihpl1-*
    ./create-hpl.py
    ./create-hnl.py
    s1=$(stat -c %s tihpl1-*)
    s2=$(stat -c %s tihnl1-*)
    echo $i $ratio $s1 $s2 >>$LOG
done

