#! /bin/bash

[ X$HOME != X ] || exit 1

LOG=$HOME/git/hcsl/experimental/log.ex-2

for i in $(seq 0 100); do
    ratio=$(perl -E "print($i/100)")
    echo $i $ratio
    cd ../certdb || exit 1
    ./gen_certdb.py 2000000 $ratio
    cd ../experimental || exit 1
    ./pl-bits.py
    ./nl-bits.py
    echo $i $ratio $(cat res-pl-bits.txt) $(cat res-nl-bits.txt) >>$LOG
done

