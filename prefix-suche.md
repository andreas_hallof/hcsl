## Experiment kürzeste Prefix

    ...
    'fdd0b', 'fdd68', 'fdddc', 'fde5',  'fdec7',  'fdf1d', 'fdf61',  'fdfd6',
    'fe046', 'fe0b1', 'fe13a', 'fe1ab', 'fe207',  'fe275', 'fe2f7',  'fe37',
    'fe3f2', 'fe443', 'fe49e', 'fe4f',  'fe5452', 'fe5c0', 'fe5f8',  'fe69',
    'fe6f6', 'fe71c', 'fe77',  'fe7b7', 'fe834',  'fe8b4', 'fe908',  'fe984',
    'fea0e', 'fea85', 'feb0',  'feb7b', 'febf0',  'fec4',  'fec8',   'fed0e',
    'fed5',  'fee0',  'fee5c', 'feeb4', 'fef1d',  'fef92', 'ff023',  'ff09',
    'ff11',  'ff152', 'ff1ed', 'ff23d', 'ff2c5',  'ff2fb', 'ff37',   'ff3c',
    'ff459', 'ff4ca', 'ff54e', 'ff607', 'ff666',  'ff710', 'ff78',   'ff7f4',
    'ff85',  'ff892', 'ff907', 'ff950', 'ff9a6b', 'ffa5',  'ffaf56', 'ffb3b',
    'ffb83', 'ffbd9', 'ffc3b', 'ffca4', 'ffd4c',  'ffd7a', 'ffde62', 'ffe46',
    'ffe93', 'ffed',  'fff37', 'fffa',  'ffff9']

     {4: 2140, 5: 6989, 6: 811, 7: 53, 8: 6, 9: 1})

### prefix-2 mit 100\_000 Zertifikaten

    3', 'ffbd9', 'ffc3b', 'ffca4', 'ffd4c', 'ffd7a', 'ffde62', 'ffe46',
    'ffe93', 'ffed', 'fff37', 'fffa', 'ffff9']

    4 : 2140
    5 : 6989
    6 : 811
    7 : 53
    8 : 6
    9 : 1

9\*4 = 45 und ceil(45/8) = 5 also 5 Byte maximal


# ./pl-prefix-3.py mit 10\_000\_000 Zertifikaten

    ./pl-prefix-3.py
    Generating Cert-DB Part 1
    100% (11000000 of 11000000) |#######################| Elapsed Time: 0:00:37 Time:  0:00:37
    Generating Cert-DB Part 2
    100% (11000000 of 11000000) |#######################| Elapsed Time: 0:00:24 Time:  0:00:24
    building prefix-cache
    100% (1100000 of 1100000) |#########################| Elapsed Time: 0:00:13 Time:  0:00:13
    100% (9900000 of 9900000) |#########################| Elapsed Time: 0:00:59 Time:  0:00:59
    Länge Filter-Array 9900000

    5 : 707143
    6 : 8471500
    7 : 676236
    8 : 42338
    9 : 2630
    10 : 145
    11 : 8
    => Bytelänge max. prefixlänge 5.50
    final check
    assertion
    len set = 7194451 len filter array= 9900000 Ratio= 0.7267122222222222
    ende

### prefix-2 mit 1\_000\_000 Zertifikaten

    ', 'fff76b', 'fff7b', 'fff81b', 'fff8b5af', 'fff900', 'fff95d', 'fffa1b',
    'fffad8f', 'fffb4e', 'fffbf4', 'fffcab', 'fffdb3', 'fffe7', 'ffff822',
    'fffff7']

    5 : 38497
    6 : 55773
    7 : 5356
    8 : 351
    9 : 23

9\*4 = 45 und ceil(45/8) = 5 also 5 Byte maximal


# prefix-3 mit 10\_000\_000 Zertifikaten

    $ ./prefix-3.py
    Generating Cert-DB Part 1
    100% (10000000 of 10000000) |#############| Elapsed Time: 0:00:33 Time:  0:00:33
    Generating Cert-DB Part 2
    100% (10000000 of 10000000) |#############| Elapsed Time: 0:00:18 Time:  0:00:18
    building prefix-cache
    100% (9000000 of 9000000) |###############| Elapsed Time: 0:01:43 Time:  0:01:43
    100% (1000000 of 1000000) |###############| Elapsed Time: 0:00:34 Time:  0:00:34
    Länge Filter-Array 1000000

    5 : 67
    6 : 551094
    7 : 412144
    8 : 34335
    9 : 2208
    10 : 142
    11 : 8
    12 : 2
    => Bytelänge max. prefixlänge 6.00
    final check
    assertion
    ende

