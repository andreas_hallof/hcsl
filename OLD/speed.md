# Raspberry Pi Model 4B

## RSA

	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ openssl speed rsa2048
	Doing 2048 bits private rsa's for 10s: 1439 2048 bits private RSA's in 9.99s
	Doing 2048 bits public rsa's for 10s: 65764 2048 bits public RSA's in 10.00s

            OpenSSL 1.1.1d  10 Sep 2019
            built on: Mon Apr 27 09:55:40 2020 UTC
            options:bn(64,32) rc4(char) des(long) aes(partial) blowfish(ptr) 
            compiler: gcc -fPIC -pthread -Wa,--noexecstack -Wall
            -D__ARM_MAX_ARCH__=7 -Wa,--noexecstack -g -O2
            -fdebug-prefix-map=/home/pi/work/new=. -fstack-protector-strong
            -Wformat -Werror=format-security -DOPENSSL_USE_NODELETE -DOPENSSL_PIC
            -DOPENSSL_CPUID_OBJ -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_GF2m
            -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DAES_ASM
            -DBSAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DPOLY1305_ASM -DNDEBUG
            -Wdate-time -D_FORTIFY_SOURCE=2

			  sign    verify    sign/s verify/s
	rsa 2048 bits 0.006942s 0.000152s    144.0   6576.4
	pi@raspberrypi:/sys/class/thermal/thermal_zone0 $ cat temp 
	35537

# Uralt-PC

mehr als 11 Jahre alt, Intel Core2 Quad Q9550 @ 2.83GHz

## RSA

	$ openssl speed rsa2048
        Doing 2048 bits private rsa's for 10s: 4325 2048 bits private RSA's in 9.99s
        Doing 2048 bits public rsa's for 10s: 145179 2048 bits public RSA's in 10.00s

            OpenSSL 1.1.1i  8 Dec 2020
            built on: Sat Dec 12 06:34:39 2020 UTC
            options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr) 
            compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -march=x86-64
            -mtune=generic -O2 -pipe -fno-plt -Wa,--noexecstack -D_FORTIFY_SOURCE=2
            -march=x86-64 -mtune=generic -O2 -pipe -fno-plt
            -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
            -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ
            -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5
            -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM
            -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM
            -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG
            -D_FORTIFY_SOURCE=2

                          sign    verify    sign/s verify/s
        rsa 2048 bits 0.002310s 0.000069s    432.9  14517.9

# laptop hp elitebook

    $ openssl.exe speed rsa2048
    Doing 2048 bits private rsa's for 10s: 16418 2048 bits private RSA's in 10.02s
    Doing 2048 bits public rsa's for 10s: 499005 2048 bits public RSA's in 10.06s

        OpenSSL 1.1.1l  24 Aug 2021
        built on: Sun Nov 28 16:52:04 2021 UTC
        options:bn(64,64) md2(char) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr)
        compiler: gcc  -ggdb -O2 -pipe -Wall -Werror=format-security
        -Wp,-D_FORTIFY_SOURCE=2 -fstack-protector-strong --param=ssp-buffer-size=4
        -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/build=/usr/src/debug/openssl-1.1.1l-2
        -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/src/openssl-1.1.1l=/usr/src/debug/openssl-1.1.1l-2
        -DTERMIOS -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2
        -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m
        -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM
        -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM
        -DPOLY1305_ASM -DZLIB -DNDEBUG -DDEVRANDOM="\"/dev/urandom\""
        -DSYSTEM_CIPHERS_FILE="/etc/crypto-policies/back-ends/openssl.config"

                   sign       verify    sign/s verify/s
    rsa 2048 bits 0.000610s 0.000020s   1639.3  49588.1


    $ openssl.exe speed rsa2048
    Doing 2048 bits private rsa's for 10s: 16435 2048 bits private RSA's in 10.00s
    Doing 2048 bits public rsa's for 10s: 493828 2048 bits public RSA's in 10.02s

        OpenSSL 1.1.1l  24 Aug 2021
        built on: Sun Nov 28 16:52:04 2021 UTC
        options:bn(64,64) md2(char) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr)
        compiler: gcc  -ggdb -O2 -pipe -Wall -Werror=format-security
        -Wp,-D_FORTIFY_SOURCE=2 -fstack-protector-strong
        --param=ssp-buffer-size=4
        -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/build=/usr/src/debug/openssl-1.1.1l-2
        -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/src/openssl-1.1.1l=/usr/src/debug/openssl-1.1.1l-2
        -DTERMIOS -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ
        -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5
        -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM -DSHA512_ASM
        -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM
        -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DZLIB -DNDEBUG
        -DDEVRANDOM="\"/dev/urandom\""
        -DSYSTEM_CIPHERS_FILE="/etc/crypto-policies/back-ends/openssl.config"

                       sign    verify    sign/s verify/s
     rsa 2048 bits 0.000608s 0.000020s   1643.5  49303.9

     $ openssl.exe speed rsa2048
     Doing 2048 bits private rsa's for 10s: 16337 2048 bits private RSA's in 10.02s
     Doing 2048 bits public rsa's for 10s: 490684 2048 bits public RSA's in 10.02s

         OpenSSL 1.1.1l  24 Aug 2021
         built on: Sun Nov 28 16:52:04 2021 UTC
         options:bn(64,64) md2(char) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr)
         compiler: gcc  -ggdb -O2 -pipe -Wall
         -Werror=format-security
         -Wp,-D_FORTIFY_SOURCE=2
         -fstack-protector-strong
         --param=ssp-buffer-size=4
         -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/build=/usr/src/debug/openssl-1.1.1l-2
         -fdebug-prefix-map=/mnt/share/cygpkgs/openssl/openssl.x86_64/src/openssl-1.1.1l=/usr/src/debug/openssl-1.1.1l-2
         -DTERMIOS -DL_ENDIAN -DOPENSSL_PIC
         -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2
         -DOPENSSL_BN_ASM_MONT
         -DOPENSSL_BN_ASM_MONT5
         -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM
         -DSHA256_ASM -DSHA512_ASM
         -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM
         -DAESNI_ASM -DVPAES_ASM -DGHASH_ASM
         -DECP_NISTZ256_ASM -DX25519_ASM
         -DPOLY1305_ASM -DZLIB -DNDEBUG
         -DDEVRANDOM="\"/dev/urandom\""
         -DSYSTEM_CIPHERS_FILE="/etc/crypto-policies/back-ends/openssl.config"
  
                        sign    verify    sign/s verify/s
      rsa 2048 bits 0.000613s 0.000020s   1631.3  48990.0

# dell latitude 7480

	andreas@andreas-Latitude-7480:~/git/hrl/graph$ openssl speed rsa2048
	Doing 2048 bits private rsa's for 10s: 15988 2048 bits private RSA's in 10.00s
	Doing 2048 bits public rsa's for 10s: 551322 2048 bits public RSA's in 10.00s
	OpenSSL 1.1.1l  24 Aug 2021
	built on: Wed Nov 24 09:53:29 2021 UTC
	options:bn(64,64) rc4(16x,int) des(int) aes(partial) blowfish(ptr)

	compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall
	-Wa,--noexecstack -g -O2
	-ffile-prefix-map=/build/openssl-1D0sn6/openssl-1.1.1l=. -flto=auto
	-ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security
	-DOPENSSL_TLS_SECURITY_LEVEL=2 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC
	-DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT
	-DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM
	-DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM
	-DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time
	-D_FORTIFY_SOURCE=2

			  sign    verify    sign/s verify/s
	rsa 2048 bits 0.000625s 0.000018s   1598.8  55132.2

	andreas@andreas-Latitude-7480:~/git/hrl/graph$ openssl speed rsa2048
	Doing 2048 bits private rsa's for 10s: 16414 2048 bits private RSA's in 10.00s
	Doing 2048 bits public rsa's for 10s: 554144 2048 bits public RSA's in 10.00s
	OpenSSL 1.1.1l  24 Aug 2021
	built on: Wed Nov 24 09:53:29 2021 UTC
	options:bn(64,64) rc4(16x,int) des(int) aes(partial) blowfish(ptr)

	compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall
	-Wa,--noexecstack -g -O2
	-ffile-prefix-map=/build/openssl-1D0sn6/openssl-1.1.1l=. -flto=auto
	-ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security
	-DOPENSSL_TLS_SECURITY_LEVEL=2 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC
	-DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT
	-DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM
	-DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM
	-DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time
	-D_FORTIFY_SOURCE=2

			  sign    verify    sign/s verify/s
	rsa 2048 bits 0.000609s 0.000018s   1641.4  55414.4

	andreas@andreas-Latitude-7480:~/git/hrl/graph$ openssl speed rsa2048
	Doing 2048 bits private rsa's for 10s: 16421 2048 bits private RSA's in 10.00s
	Doing 2048 bits public rsa's for 10s: 558128 2048 bits public RSA's in 10.00s
	OpenSSL 1.1.1l  24 Aug 2021
	built on: Wed Nov 24 09:53:29 2021 UTC
	options:bn(64,64) rc4(16x,int) des(int) aes(partial) blowfish(ptr)

	compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -Wall
	-Wa,--noexecstack -g -O2
	-ffile-prefix-map=/build/openssl-1D0sn6/openssl-1.1.1l=. -flto=auto
	-ffat-lto-objects -fstack-protector-strong -Wformat -Werror=format-security
	-DOPENSSL_TLS_SECURITY_LEVEL=2 -DOPENSSL_USE_NODELETE -DL_ENDIAN -DOPENSSL_PIC
	-DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2 -DOPENSSL_BN_ASM_MONT
	-DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM -DSHA256_ASM
	-DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM -DVPAES_ASM
	-DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG -Wdate-time
	-D_FORTIFY_SOURCE=2 sign    verify    sign/s verify/s

	rsa 2048 bits 0.000609s 0.000018s   1642.1  55812.8

# 15\_000\_000 Zertifikate

	4 : 150957
	5 : 10889039
	6 : 3399804
	7 : 243782
	8 : 15368
	9 : 988
	10 : 59
	11 : 3
	=> Bytelänge max. prefixlänge 5.50
	final check
	assertion
	len set = 3293976 len filter array= 14700000 Ratio= 0.22408
	Durchlauf mit 0.99*15000000 gesperrten Zertifikaten
	Generating Cert-DB Part 2
	building prefix-cache
	100% (150000 of 150000) |######################################| Elapsed Time: 0:00:00 Time:  0:00:00
	100% (14850000 of 14850000) |##################################| Elapsed Time: 0:00:37 Time:  0:00:37
	Länge Filter-Array 14850000

	4 : 1534641
	5 : 11335571
	6 : 1848550
	7 : 122968
	8 : 7768
	9 : 468
	10 : 33
	11 : 1
	=> Bytelänge max. prefixlänge 5.50
	final check
	assertion
	len set = 2160945 len filter array= 14850000 Ratio= 0.14551818181818182
	ende

	real    137m42,024s
	user    131m14,587s
	sys     5m53,217s
	andreas@andreas-Latitude-7480:~/git/hrl/graph$
