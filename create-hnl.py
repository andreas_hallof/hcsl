#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Create HNL using the data from certdb/{good.db,revoked.db}

"""

import os, hashlib, sys, time

from binascii import hexlify, unhexlify
from progressbar import progressbar

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import utils


if __name__ == '__main__':

    AKI = "54:bc:93:f4:47:a3:63:a6:28:9e:11:df:15:0c:a9:3f:91:55:07:1d".replace(":","")
    SKI = "3a:f9:f5:c5:8c:2c:0e:e8:6a:85:ed:7c:44:19:07:ff:72:bb:ec:b8".replace(":","")
    assert len(AKI) == 20*2; assert len(SKI) == 20*2
    TIME = int(time.time())

    with open("certdb/good.db", "rb") as good_file:
        data = good_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        good = []
        for i in progressbar(range(0, nr)):
            good.append(data[i*32:(i+1)*32])
            #assert len(good[-1]) == 32

    with open("certdb/revoked.db", "rb") as revoked_file:
        data = revoked_file.read()
        assert len(data) % 32 == 0
        nr = len(data) >> 5
        revoked = []
        for i in progressbar(range(0, nr)):
            revoked.append(data[i*32:(i+1)*32])
            #assert len(revoked[-1]) == 32

    revoked.append(b"\x00"*32)
    revoked.append(b"\xff"*32)

    print("Sanity Check: sind die good-Menge und die revoked-Menge wirklich disjunkt")
    revoked_set = set(revoked)
    for element in progressbar(good):
        if element in revoked_set:
            sys.exit("ACHTUNG: Mengen nicht disjunkt!")

    hlen = 1
    conflict = True
    while conflict:
        print("Prefixlen", hlen)
        good_prefix_set = set()
        for element in progressbar(good):
            good_prefix_set.add(element[0:hlen])
        print("positiv prefix set size:", len(good_prefix_set))
        test_conflict = False
        for element in progressbar(revoked):
            if element[0:hlen] in good_prefix_set:
                test_conflict = True
                break
        if test_conflict:
            hlen += 1
        else:
            conflict = False

    print("Final:", hlen)

    my_set = set()
    for element in progressbar(revoked):
        my_set.add(element[0:hlen])

    my_list = sorted(my_set)

    print("Hauptteil hat", len(my_list), "Elemente")

    print("So die Daten sind berechnet " + \
          "... Ich erzeuge jetzt die Datenstukturen und die zwei Signaturen ...")

    header = b"TIHNL1" + \
             TIME.to_bytes(8, byteorder='big', signed=False) + \
             unhexlify(AKI) + \
             unhexlify(SKI) + \
             hlen.to_bytes(1, byteorder="big", signed=False) 
    nr = len(my_list)
    body_0 = nr.to_bytes(8, byteorder="big", signed=False)
    body_1 = bytearray(nr*hlen)
    counter = 0
    for element in progressbar(my_list):
        body_1[counter:counter+hlen] = element
        counter += hlen
    body = body_0 + body_1

    print("Signatur 1 erstellen")
    with open("pki/ocsp_key.pem", "rb") as key_file:
        serialized_private_key=key_file.read()
    private_key = serialization.load_pem_private_key(serialized_private_key,
                              password=None, backend=default_backend() )
    signature_1 = private_key.sign(hashlib.sha256(header+body).digest(),
                                ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    print("Signatur 2 erstellen")
    print("L_0 erstellen")
    ende_liste = []
    # ich weiss, dass wegen dem 0-Element und dem 1-Element gelten muss
    # len(my_liste)>1, trotzdem nochmal sanity-check
    assert len(my_list)>1
    for i in progressbar(range(0,len(my_list)-1)):
        ende_liste.append(hashlib.sha256(my_list[i]+my_list[i+1]).digest())

    while len(ende_liste)>1:
        start_liste = ende_liste
        l_s = len(start_liste)

        ende_liste = []
        for i in range(0, l_s if l_s & 1 == 0 else l_s-1, 2):
            ende_liste.append(hashlib.sha256(start_liste[i] + start_liste[i+1]).digest())
        if l_s & 1 == 1:
            ende_liste.append(start_liste[-1])

    signature_2 = private_key.sign(hashlib.sha256(header+ende_liste[0]).digest(),
                                ec.ECDSA(utils.Prehashed(hashes.SHA256())))

    signature = signature_1 + signature_2

    with open(f"tihnl1-{TIME}-{AKI}", "wb") as hnl_file:
        hnl_file.write(header + body + signature)

