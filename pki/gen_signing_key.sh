#! /usr/bin/env bash

# https://legacy.thomas-leister.de/eine-eigene-openssl-ca-erstellen-und-zertifikate-ausstellen/
# https://0xc45.com/blog/openssl-certificate-authority/

openssl ecparam -name prime256v1 -genkey -out ca_key.pem

openssl req -x509 -new -nodes -key ca_key.pem \
-out ca_cert.pem -days $((365*8)) \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=HPL-HNL-Example CA"


openssl ecparam -name prime256v1 -genkey -out ocsp_key.pem

openssl req -new -key ocsp_key.pem -out ocsp_cert.csr \
-subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=HPL-HNL-Example OCSP-Responder"

#openssl x509 -req -in ocsp_cert.csr -CA ca_cert.pem -CAkey ca_key.pem \
#-CAcreateserial -out ocsp_cert.pem -days $((365*5)) 

openssl ca -config ca.cnf -extensions leaf_extensions -out ocsp_cert.pem -infiles ocsp_cert.csr

