#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import datetime, hashlib, json, math, sys, time
import matplotlib.pyplot as plt

from collections import defaultdict
#from progressbar import progressbar 


if __name__ == '__main__':
    
# report-10M.txt report-150K.txt report-15M.txt report-1M.txt report-300K.txt report-5M.txt

    #for job_number in ["150K", "300K", "1M", "5M", "10M", "15M"]:
    for job_number in ["1M"]:

        print("Starting", job_number)

        anzahl_zertifikate = 0
        proz = []; 
        filter_size = []

        with open("report-sim-"+job_number+".txt", "rt") as data_file:
            for line in data_file:
                data = json.loads(line)
                anzahl_zertifikate = data["anzahl"]
                proz.append(data["prozent"])
                filter_size.append(int(data["byte_len_filter"]/1024.0))

        #fig = plt.figure()
        #mytext = fig.add_subplot()
        #mytext.text(0.8, 600, f"TSP mit {job_number} Zertifikaten")

        plt.plot(proz,filter_size)
        plt.xlabel('Anteil gesperrter Zertifikate')
        plt.ylabel('Größe Filter in KiB')
        plt.text(0.1, 3000, f"TSP mit {job_number} Zertifikaten")
        plt.savefig("g-sim-"+job_number+".png")
        plt.clf()

        print("Done", job_number)

