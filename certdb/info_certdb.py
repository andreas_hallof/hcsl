#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

if __name__ == '__main__':

    with open("good.db", "ab") as good_file:
        good_file_size = good_file.tell()
        assert good_file_size % 32 == 0
        nr_good = good_file_size >> 5

    with open("revoked.db", "ab") as revoked_file:
        revoked_file_size = revoked_file.tell()
        assert revoked_file_size % 32 == 0
        nr_revoked = revoked_file_size >> 5

    nr_certs = nr_good + nr_revoked
    print("#good    =", nr_good)
    print("#revoked =", nr_revoked)
    print("ratio={}".format(nr_revoked/float(nr_certs)))

