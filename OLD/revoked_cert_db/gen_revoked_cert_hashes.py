#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Zum Testen ein paar Hashwerte gesperrter Zertifikate erzeugen.

"""

import os

from binascii import hexlify

if __name__ == '__main__':
    
    number = 2

    for i in range(1, number+1):
        data = i.to_bytes(length=32, byteorder='little', signed=False)
        name = hexlify(data).decode()
        print(name)
        if not os.path.exists(name):
            open(name, "a").close()

