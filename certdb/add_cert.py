#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Fügt einen Hashwert (also ein Zertifikat) zur angegebenen
db-Datei hinzu.

"""

import sys

if __name__ == '__main__':

    if len(sys.argv) >= 3:
        (filename, nr_certs) = (sys.argv[1], int(sys.argv[2]))
        assert nr_certs>0
    else:
        filename = "good.db"
        nr_certs = 1

    print(f"adding {nr_certs} element(s) to {filename}")

    with open(filename, "ab") as my_file:
        with open("/dev/urandom", "rb") as random_source:
            data = random_source.read(nr_certs*32)
            assert len(data) == nr_certs*32
            my_file.write(data)
            print("Filesize now", my_file.tell())

