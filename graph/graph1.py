#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import datetime, hashlib, json, math, sys, time
import matplotlib.pyplot as plt

from collections import defaultdict
#from progressbar import progressbar 


if __name__ == '__main__':
    
# report-10M.txt report-150K.txt report-15M.txt report-1M.txt report-300K.txt report-5M.txt

    for job_number in ["150K", "300K", "1M", "5M", "10M", "15M"]:

        print("Starting", job_number)

        anzahl_zertifikate = 0
        proz = []; 
        revoked = []
        filter_elements = []

        with open("report-"+job_number+".txt", "rt") as data_file:
            for line in data_file:
                data = json.loads(line)
                anzahl_zertifikate = data["anzahl"]
                proz.append(data["prozent"])
                revoked.append(data["l_ar"])
                filter_elements.append(data["l_set"])

        plt.plot(proz, revoked)
        plt.plot(proz,filter_elements)
        plt.xlabel('Anteil gesperrter Zertifikate')
        plt.ylabel('Anzahl Elemente im Filter/Postivliste')
        plt.savefig("g-"+job_number+".pdf")
        plt.clf()

        print("Done", job_number)

