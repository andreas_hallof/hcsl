#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Ich erzeuge Zertifikatshashwerte zum Testen.

"""

import hashlib, sys

from collections import defaultdict
from progressbar import progressbar 

if __name__ == '__main__':

    if len(sys.argv) == 3:
        nr_certs = int(sys.argv[1]); assert nr_certs>1
        sperrrate = float(sys.argv[2]); assert sperrrate>=0.0 and sperrrate<=1.0
    elif len(sys.argv) == 1:
        nr_certs = 150_000*3
        sperrrate = 0.1
    else:
        print("Ich erwarte ./gen_certdb.py Anzahl-der-Zertikate Sperrrate")
        sys.exit(1)

    print(f"using parameters: nr#cert={nr_certs} revoked_ratio={sperrrate}")

    nr_revoked = int(nr_certs*sperrrate)
    with open("good.db", "wb") as good_file:
        for i in progressbar(range(0, nr_certs-nr_revoked)):
            good_file.write(hashlib.sha256(f"G{i}".encode()).digest())

    with open("revoked.db", "wb") as revoked_file:
        for i in progressbar(range(0, nr_revoked)):
            revoked_file.write(hashlib.sha256(f"R{i}".encode()).digest())
    
    print("#good    =", nr_certs-nr_revoked)
    print("#revoked =", nr_revoked)
    print("ratio    =", nr_revoked/float(nr_certs))

