#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, glob, os, json, sys, tomli

from base64 import b16encode, b64encode
from collections import defaultdict
from cryptography import x509
from cryptography.hazmat.backends import default_backend


if __name__ == '__main__':

    if len(sys.argv)<2:
        sys.exit("FEHLER: benötige cert-der files als argv")

    h2n = defaultdict(lambda: "UNKNOWN")
    h2n["None"] = "selfsigned"
    ausgabe = []

    configfileupdate = False
    assert os.environ.get("HOME")
    configfilename = os.path.join(os.environ.get("HOME"),".certinfo.toml")
    print(configfilename)
    if os.path.exists(configfilename) and os.path.isfile(configfilename):
        with open(configfilename, "rb") as configfile:
            certdb = tomli.load(configfile)
            configfileupdate = True

    for cert_filename in sys.argv[1:]:
        if not (os.path.exists(cert_filename) and os.path.isfile(cert_filename)):
            sys.exit(f"unable to read from {cert_filename}")

        with open(cert_filename, "rb") as cert_file:
            cert_data = cert_file.read()
            if cert_filename.endswith(".pem"):
                cert = x509.load_pem_x509_certificate(cert_data, default_backend())
                cert_data_pem = cert_data.decode()
                pos = cert_data_pem.find("-----BEGIN CERTIFICATE-----")
                assert pos >= 0
                cert_data_pem = cert_data_pem[pos:]
            else: 
                cert = x509.load_der_x509_certificate(cert_data, default_backend())
                cert_data_pem = "-----BEGIN CERTIFICATE-----\n" + b64encode(cert_data) + \
                                "\n-----END CERTIFICATE-----\n"
            ski = None
            aki = None
            for ext in cert.extensions:
                if ext.oid._name == 'subjectKeyIdentifier':
                    ski = ext.value.digest
                    # erst ab cryptography 35.0 kann man anstatt digest
                    # auch key_identifier nehmen
                elif ext.oid._name == 'authorityKeyIdentifier':
                    aki = ext.value.key_identifier

            assert ski
            ski_hex = b16encode(ski).decode().lower()
            if configfileupdate:
                if not(ski_hex in certdb):
                    print(f"adding to {configfilename}")
                    with open(configfilename, "at") as configfile:
                        configfile.write(ski_hex+'= """\n'+cert_data_pem+ '"""\n')

            ski = b16encode(ski).decode()
            if aki:
                aki = b16encode(aki).decode()

            cn = None
            for subject_element in cert.subject:
                if subject_element.oid.dotted_string == '2.5.4.3':
                    cn = subject_element.value
            assert cn

            if ski in h2n:
                if h2n[ski] != cn:
                    print("WARNING: Neudefinition")
            else:
                h2n[ski] = cn

            ausgabe.append([cn, ski, aki])

    for [cn, ski, aki] in ausgabe:
         print(cn, ski, h2n[ski], aki, h2n[aki])

