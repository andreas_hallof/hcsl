#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Create Diff-File from two H{P,N}L-Files

"""

import sys

from binascii import hexlify

from progressbar import progressbar

if __name__ == '__main__':

    if len(sys.argv) != 3:
        print("Usage: create-diff.py SRC-FILE DST-FILE")
        sys.exit(0)

    (src_filename, dst_filename) = sys.argv[-2:]

    with open(src_filename, "rb") as src_file:
        src_type = src_file.read(6)
        assert (src_type == b"TIHPL1") or (src_type == b"TIHNL1")
        src_time_bytes = src_file.read(8)
        src_time = int.from_bytes(src_time_bytes, byteorder="big", signed=False)
        src_aki = src_file.read(20)
        src_ski = src_file.read(20)
        src_hlen = int.from_bytes(src_file.read(1), byteorder="big", signed=False)
        src_nr_elements = int.from_bytes(src_file.read(8), byteorder="big", signed=False)
        src_data_block = src_file.read(src_hlen*src_nr_elements)
        assert len(src_data_block) == src_hlen*src_nr_elements
        # jetzt kommt im src_file die Signatur am Ende 
        # im aktuellen Kontext ist die für mich unwichtig

    with open(dst_filename, "rb") as dst_file:
        dst_type = dst_file.read(6)
        assert (dst_type == b"TIHPL1") or (dst_type == b"TIHNL1")
        dst_time = int.from_bytes(dst_file.read(8), byteorder="big", signed=False)
        dst_aki = dst_file.read(20)
        dst_ski = dst_file.read(20)
        dst_hlen = int.from_bytes(dst_file.read(1), byteorder="big", signed=False)
        dst_nr_elements = int.from_bytes(dst_file.read(8), byteorder="big", signed=False)
        dst_data_block = dst_file.read(dst_hlen*dst_nr_elements)
        assert len(dst_data_block) == dst_hlen*dst_nr_elements
        trailer = dst_file.read()
        assert len(trailer)>32*4
        dst_file.seek(0)
        header_len = 6+8+20+20+1+8
        dst_header = dst_file.read(header_len); assert len(dst_header) == header_len

    print("starttime =", src_time)
    print("dst-time  =", dst_time)

    assert src_type == dst_type
    type_name = dst_type.decode().lower()
    print("type      =", type_name)
    
    assert src_hlen == dst_hlen
    print("hlen      =", src_hlen)

    assert src_aki == dst_aki
    aki_name = hexlify(dst_aki).decode()
    print("aki       =", aki_name)

    dst_set = set()
    for i in progressbar(range(0, dst_nr_elements)):
        dst_set.add(dst_data_block[i*dst_hlen:(i+1)*dst_hlen])
    assert len(dst_set) == dst_nr_elements

    diff_array = []
    src_set = set()
    for i in progressbar(range(0, src_nr_elements)):
        element = src_data_block[i*src_hlen:(i+1)*src_hlen]
        src_set.add(element)
        if not element in dst_set:
            diff_array.append(b"R" + element)
    assert len(src_set) == src_nr_elements
    # so dst_set brauche ich erst mal nicht
    dst_set = None

    to_remove = len(diff_array)
    print(to_remove, "elements must be removed")

    for i in progressbar(range(0, dst_nr_elements)):
        element = dst_data_block[i*dst_hlen:(i+1)*dst_hlen]
        if not element in src_set:
            diff_array.append(b"A" + element)

    # so src_set brauche ich erst mal nicht
    src_set = None

    hlen = src_hlen # == dst_hlen
    print(len(diff_array)-to_remove, "elements must be added")
    new_data_block = bytearray(len(diff_array)*(hlen+1))
    counter = 0
    for element in progressbar(diff_array):
        assert len(element) == hlen + 1
        new_data_block[counter:counter+hlen+1] = element
        counter += hlen + 1

    with open("-".join(["tidiff1", type_name, 
                        str(src_time), str(dst_time), 
                        aki_name]), "wb") as output_file:
        output_file.write(b"tidiff1-tihpl1" + src_time_bytes + dst_header)
        nr_elements = len(new_data_block)
        output_file.write(nr_elements.to_bytes(8, byteorder="big", signed=False) + new_data_block)
        output_file.write(trailer)
        print("total size of diff-file:", output_file.tell())

