#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import hashlib, math, sys, time

from binascii import hexlify
from collections import defaultdict
from progressbar import progressbar 

if __name__ == '__main__':

    MAX = 1_000_000

    hashlist = [hashlib.sha256(f"{i}".encode()).hexdigest() for i in range(0,MAX)]

    good = []; revoked = []; i = 1;
    for certhash in sorted(hashlist):
        if i % 10 == 0:
            revoked.append(certhash)
        else:
            good.append(certhash)
        i += 1

    max_cache_letters = 12
    prefix_cache = [set() for i in range(0,max_cache_letters)]
    print("building prefix-cache")
    for current_cert_hash in progressbar(good):
        for i in range(0,max_cache_letters):
            prefix_cache[i].add(current_cert_hash[:i+1])

    filter_ar = []
    for current_cert_hash in progressbar(revoked):
        my_filter = ""
        found_prefix_is_ok = False
        for letter in current_cert_hash[:max_cache_letters]:
            my_filter += letter
            if not (my_filter in prefix_cache[len(my_filter)-1]):
                found_prefix_is_ok = True
                break
        assert found_prefix_is_ok
        filter_ar.append(my_filter)

    print(len(filter_ar))
    print(filter_ar)

    histogramm = defaultdict(lambda: 0)
    for my_item in filter_ar:
        histogramm[len(my_item)]+=1
    print()
    for key in sorted(histogramm):
        print(key,":", histogramm[key])

    test_uniq = set(filter_ar)
    assert len(filter_ar) == len(test_uniq)

