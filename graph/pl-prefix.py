#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
xxx

"""

import datetime, hashlib, json, math, sys, time

from binascii import hexlify
from collections import defaultdict
from progressbar import progressbar 


def durchlauf(prozent: float)-> bool:

    assert (prozent >= 0) and (prozent <= 1)

    print(f"Durchlauf mit {prozent}*{MAX} gesperrten Zertifikaten")

    print("Generating Cert-DB Part 2")
    grenze = math.ceil(len(hashlist)*prozent)
    good = hashlist[:grenze]
    revoked = hashlist[grenze:]

    max_cache_letters = 14
    prefix_revoked_cache = [set() for i in range(0,max_cache_letters)]
    print("building prefix-cache")
    for current_cert_hash in progressbar(revoked):
        for i in range(0,max_cache_letters):
            prefix_revoked_cache[i].add(current_cert_hash[:i+1])

    # 'revoked' brauche ich jetzt nicht mehr
    revoked = None

    filter_ar = []
    for current_cert_hash in progressbar(good):
        my_filter = ""
        found_prefix_is_ok = False
        for letter in current_cert_hash[:max_cache_letters]:
            my_filter += letter
            if not (my_filter in prefix_revoked_cache[len(my_filter)-1]):
                found_prefix_is_ok = True
                break
        assert found_prefix_is_ok
        filter_ar.append(my_filter)

    print("Länge Filter-Array", len(filter_ar))
    #print(filter_ar)

    # 'good' brauche ich jetzt nicht mehr
    good = None

    histogramm = defaultdict(lambda: 0)
    for my_item in filter_ar:
        histogramm[len(my_item)]+=1
    print()
    for key in sorted(histogramm):
        print(key,":", histogramm[key])

    max_prefix_len = sorted(histogramm)[-1]
    print("=> Bytelänge max. prefixlänge", "{:.2f}".format(max_prefix_len/2.0))

    print("final check")
    test_uniq = set(filter_ar)

    l_set = len(test_uniq)
    l_ar = len(filter_ar)
    print("assertion")
    print("len set =", l_set, "len filter array=", l_ar, "Ratio=", l_set/float(l_ar))

    with open("report-" + start_date, "at+") as report_file:
        report_file.write(json.dumps({
            "anzahl" : len(hashlist),
            "prozent" : prozent, "l_set" : l_set, "l_ar" : l_ar,
            "ratio" : l_set/float(l_ar), "max_prefix_len" : max_prefix_len}) + "\n")

    return True


if __name__ == '__main__':

    MAX = 15_000_000

    start_date = datetime.datetime.now().isoformat()
    print("Generating Cert-DB Part 1")
    hashlist = [hashlib.sha256(f"{i}".encode()).hexdigest() \
                 for i in progressbar(range(0,MAX))]

    for i in range(1,100):
        durchlauf(i/100.0)


    print("ende")
